import Vue from 'vue'
import VueRouter from 'vue-router'
const ServerSide = () => import('./../components/serverSideSample')
const TestTable1 = () => import('./../components/fullTestInvoice')
const ReportMode = () => import('./../components/reportMode')
const email = () => import('../components/InvoiceEmailTemplate')
const invoiceDetails = () => import('../components/inv')
const date = () => import('../components/date')
const csv = () => import('../components/csv')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'TestTable1',
    component: TestTable1
  },
  {
    path: '/date',
    name: 'date',
    component: date
  },
  {
    path: '/',
    name: 'TestTable1',
    component: TestTable1
  },
  {
    path: '/inv',
    name: 'invoiceDetails',
    component: invoiceDetails
  },
  {
    path: '/report',
    name: 'ReportMode',
    component: ReportMode
  },
   {
    path: '/csvTest',
    name: 'csv',
    component: csv
  },
  {
    path: '/email',
    name: 'email',
    component: email
  },
  {
    path: '/server',
    name: 'ServerSide',
    component: ServerSide
  },


]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
