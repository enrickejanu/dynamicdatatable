import axios from 'axios'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['authority'] = 'app.schedeasy.net';

axios.defaults.headers.post['path'] = '/AdminConsole/Schedeasy/1605150584/franchise/jobs/viewAll';
axios.defaults.headers.post['scheme'] = 'https';
axios.defaults.headers.post['scheme'] = 'https';
axios.defaults.headers.post['cookie'] =  'same-site-cookie=foo; cross-site-cookie=bar; schedeasy-_zldp=8RhI%2FKYF8NCUYhATVi6LrBxruHRVpnI8onhmEOxCBCpp7FrALrVD12nsQvdiEuDSodoRkLyJC2Y%3D; schedeasynet-_zldp=SAXvWp3DJctv8IVO4RkhLJrM%2Fl2bB%2BF2YypsgfspKeDD%2BGcP%2Bl9O2GJZyXCQrcFHodoRkLyJC2Y%3D; _ga=GA1.2.1673417192.1625809647; _ga_BCP77PMC3K=GS1.1.1629972163.17.0.1629972168.0; _gid=GA1.2.521527893.1630634860; same-site-cookie=foo; cross-site-cookie=bar; _gat_gtag_UA_174567066_1=1; XSRF-TOKEN=eyJpdiI6InhZSHBcL1FEcWMrNjIxMkFzZ01hVGZBPT0iLCJ2YWx1ZSI6ImFpZUFiWFhwYUhLT2E0dEZxVGRQMnZUY01PUTBwa01ZTk1zdVFZWklDSHpWRWZmcGwydTQ5eHBRcmY0b0JwOWciLCJtYWMiOiI1YTMxY2I5MmVhMjI1Y2Y0ODEyZGUyMDE5MTlkNzRkYWU1NGY4YjU4ZDgxOWIyYzA5OGRlZmViM2U3OTJiNmNjIn0%3D; laravel_session=eyJpdiI6IjVMN29LT3VtdHhRd3RsVmdBekFZd2c9PSIsInZhbHVlIjoiS0dZU1A0YWtTdmpUTDNwZUQzSEs1dkpyTWN4SEVSOFgzN28rRlRFVVRndW5vYW9DOUwra1pwTlNESWJlVjhZRyIsIm1hYyI6IjMzZjk3Nzk2MDA2Y2ZiNWEzYzQyYjE4ZTg4ZTQxMDJmOWUyZjExYjhiYTYxNWZjMWUzNjU0NzE0NjE2NGE4NjcifQ%3D%3D'
var isDevelopment = false;

const instance = axios.create({

});

export default instance
// baseURL: 'http://localhost:8000/api/',
// baseURL: 'http://api.codingfork.com/api/',
