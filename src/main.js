import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import _ from 'lodash'
import VuePapaParse from 'vue-papa-parse'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import locale from 'element-ui/lib/locale/lang/en'

import moment from "moment";
import VueDatePicker from '@mathieustan/vue-datepicker';
import '@mathieustan/vue-datepicker/dist/vue-datepicker.min.css';
import {VueCsvImportPlugin} from "vue-csv-import";
Vue.use(VueCsvImportPlugin);
Vue.use(VuePapaParse)

Vue.use(VueDatePicker);
Vue.prototype.moment = moment
Vue.config.productionTip = false
Vue.use(ElementUI, { locale })
Vue.use(require('moment'))
Object.defineProperty(Vue.prototype, '$_', { value: _ })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
