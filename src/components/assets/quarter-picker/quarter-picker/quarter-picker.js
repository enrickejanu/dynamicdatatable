import Picker from './picker.vue'
import QuarterPanel from './quarter-panel.vue'

export default {
  mixins: [Picker],

  props: {
    format: {
      type: String,
      default: 'yyyyQq'
    },
    valueFormat: {
      type: String,
      default: 'yyyyQq'
    },
    type: {
      type: String,
      default: 'quarter'
    }
  },

  name: 'ElQuarterPicker',

  created () {
    this.panel = QuarterPanel
  }
}
