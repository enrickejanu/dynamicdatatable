import moment from "moment";

export const dynamicTableDat = {

}

export const presetsMulti = [
  {
    table:"invoice",
    date:"week",
    name:"preset 1",
    fields:[
      {
        label:"Invoice Id",
        value:"invoice_id",
      },
      {
        label:"Postal",
        value:"postal",
      },
      {
        label:"Location",
        value:"location",
      }
    ],
    searchFilter:[
      {
        label: "postal",
        value: "6000",
        column: "postal"
      },
      {
        label: "location",
        value: "cebu",
        column: "location"
      },
      {
        label: "location",
        value: "mandaue",
        column: "location"
      },
      {
        label: "postal",
        value: "6500",
        column: "postal"
      },
    ]
  }
]

export const optionDate = [
  // {
  //     label:"Preset 1",
  //     value:{
  //         table:"invoice",
  //         date:"week",
  //         name:"preset 1",
  //         fields:[
  //             {
  //               label:"Invoice Id",
  //               value:"invoice_id",
  //             },
  //             {
  //               label:"Postal",
  //               value:"postal",
  //             },
  //             {
  //               label:"Location",
  //               value:"location",
  //             }
  //         ],
  //         searchFilter:[
  //             {
  //               label: "postal",
  //               value: "6000",
  //               column: "postal"
  //             },
  //             {
  //               label: "location",
  //               value: "cebu",
  //               column: "location"
  //             },
  //             {
  //               label: "location",
  //               value: "mandaue",
  //               column: "location"
  //             },
  //             {
  //               label: "postal",
  //               value: "6500",
  //               column: "postal"
  //             },
  //         ]
  //       }
  // },
  //
  {
    label:"Invoices by payment status (Today)",
    value:{
        table:"invoice",
        date:"date",
        name:"Invoices by payment status (Day)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Status",
              value:"payment_status",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Invoices by payment status (This Week)",
    value:{
        table:"invoice",
        date:"week",
        name:"Invoices by payment status (Week)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Status",
              value:"payment_status",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Invoices by payment status (This Month)",
    value:{
        table:"invoice",
        date:"month",
        name:"Invoices by payment status (Month)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Status",
              value:"payment_status",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Invoices by payment status (This Year)",
    value:{
        table:"invoice",
        date:"year",
        name:"Invoices by payment status (Year)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Status",
              value:"payment_status",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  //
  {
    label:"Invoices by payment Type (Today)",
    value:{
        table:"invoice",
        date:"date",
        name:"Invoices by payment Type (Day)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Method",
              value:"payment_method",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },


  },
  {
    label:"Invoices by payment Type (This Week)",
    value:{
        table:"invoice",
        date:"week",
        name:"Invoices by payment Type (Week)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Method",
              value:"payment_method",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Invoices by payment Type (This Month)",
    value:{
        table:"invoice",
        date:"month",
        name:"Invoices by payment Type (Month)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Method",
              value:"payment_method",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Invoices by payment Type (This Year)",
    value:{
        table:"invoice",
        date:"year",
        name:"Invoices by payment Type (Year)",
        fields:[
            {
              label:"Invoice Id",
              value:"invoice_id",
            },
            {
              label:"Payment Method",
              value:"payment_method",
            },
            {
              label:"Total",
              value:"total",
            },
            {
              label:"paid",
              value:"paid",
            },
            {
              label:"balance",
              value:"balance",
            },
      ],
      searchFilter:[]
    },
  },
  //
  {
    label:"Total jobs per staff",
    value:{
        table:"jobs",
        date:"date",
        name:"Total jobs per staff",
        fields:[
            {
              label:"Job Id",
              value:"job_id",
            },
            {
              label:"Staff",
              value:"staff",
            },
            {
              label:"Total Jobs",
              value:"total_jobs",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Total jobs per team",
    value:{
        table:"jobs",
        date:"date",
        name:"Total jobs per team",
        fields:[
            {
              label:"Job Id",
              value:"job_id",
            },
            {
              label:"Team",
              value:"team",
            },
            {
              label:"Total Jobs",
              value:"total_jobs",
            },
      ],
      searchFilter:[]
    },
  },
  {
    label:"Total jobs per Franchise",
    value:{
        table:"jobs",
        date:"date",
        name:"Total jobs per franchise",
        fields:[
            {
              label:"Job Id",
              value:"job_id",
            },
            {
              label:"Franchise",
              value:"franchise",
            },
            {
              label:"Total Jobs",
              value:"total_jobs",
            },
      ],
      searchFilter:[]
    },
  },
  //
  {
    label:"Total hours per job",
    value:{
        table:"jobs",
        date:"date",
        name:"Total hours per job",
        fields:[
            {
              label:"Job Id",
              value:"job_id",
            },
            {
              label:"Job Name",
              value:"job_name",
            },
            {
              label:"Total Hrs",
              value:"total_hrs",
            },
      ],
      searchFilter:[]
    },
  },
  //
  {
    label:"Average hours per job",
    value:{
        table:"jobs",
        date:"date",
        name:"Average hours per job",
        fields:[
            {
              label:"Job Id",
              value:"job_id",
            },
            {
              label:"Job Name",
              value:"job_name",
            },
            {
              label:"Average Hrs",
              value:"ave_hrs",
            },
      ],
      searchFilter:[]
    },
  },
]