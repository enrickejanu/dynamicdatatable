export const dynamicTableProps ={
     config:{
        type:Object,
        default() {
          return {};
        },
      },
      pillsServerSide:{
        type:Boolean,
        default:false
      },
      loadingText:{
        type:String,
        default:"Please Wait a moment"
      },
      serverSide:{
        type:Boolean,
        default:false
      },
      onReportMode: {
        type: Boolean,
        default: false,
      },
      reportModeData:{
        type:Array,
        default:()=>[]
      },
      customField: {
        type: Boolean,
        default: false,
      },
      hasDownload: {
        type: Boolean,
        default: false,
      },
      actionButton: {
        type: Array,
        default: () => [],
      },
      enableColumnSearch: {
        type: Boolean,
        default: false,
      },
      buttonHeader: {
        type: Array,
        default: () => [],
      },
      rowSelectActions: { type: Array },
      hasheader: {
        type: Boolean,
        default: false,
      },
      tableHeaderData: {
        type: Object,
        default() {
          return {
            clickabletags: [],
            columnName: "",
          };
        },
      },
      paginationServerSide: {
        type: Boolean,
        default: false,
      },
      hasPagination: {
        type: Boolean,
        default: true,
      },
      tableTitle: {
        type: String,
        default: "",
      },




      hasColumnSearch: {
        type: Boolean,
        default: false,
      },

      optionColumnFilter: {
        type: Array,
        default: () => [],
      },
      loading:{
        type: Boolean,
        default: false,
      },
      tableFields: {
        type: Array,
        default: () => [],
      },
      tableData: {
        type: Array,
        default: () => [],
      },
      tableSetting: {
        type: Object,
        default() {
          return {
            enableMultiSelect: false,
            enableClickableRows: false,
            rowAction: [],
          };
        },
      },
      serverSideTableLength:{
        type:Number,
        default:0
      },
      hasMultipleFilter:{
        type:Boolean,
        default:false
      },
      toggleServerSide:{
        type:Object,
        default() {
          return {
            toggled: false,
            label: "",
            header:[]
          };
        },
      },
      extraColumn:{
        type:Array,
        default: () => [],
      }
}