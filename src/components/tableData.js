export const nextDummyCol =[
  {
    sortable: true,
    label: "Job #",
    value: "job_id",
    type: "string",
  },
  {
    sortable: true,
    label: "Work##@",
    value: "work",
    type: "string",
  },
  {
    sortable: true,
    label: "PO",
    value: "po",
    type: "string",
  },
]
export const nextDummyData =[
  {
    job_id:"01",
    work:"w01",
    po:"po01"
  },
  {
    job_id:"02",
    work:"w02",
    po:"po02"
  },
  {
    job_id:"03",
    work:"w03",
    po:"po03"
  },
  {
    job_id:"04",
    work:"w04",
    po:"po04"
  },
  {
    job_id:"05",
    work:"w05",
    po:"po05"
  },
  {
    job_id:"06",
    work:"w06",
    po:"po06"
  },
  {
    job_id:"07",
    work:"w07",
    po:"po07"
  },
  {
    job_id:"08",
    work:"w08",
    po:"po08"
  },
  {
    job_id:"09",
    work:"w09",
    po:"po09"
  },
  {
    job_id:"10",
    work:"w10",
    po:"po10"
  },
  {
    job_id:"11",
    work:"w11",
    po:"po11"
  },
  {
    job_id:"12",
    work:"w12",
    po:"po12"
  },
  {
    job_id:"13",
    work:"w13",
    po:"po13"
  },
  {
    job_id:"14",
    work:"w14",
    po:"po14"
  },
  {
    job_id:"15",
    work:"w15",
    po:"po15"
  },
  {
    job_id:"16",
    work:"w16",
    po:"po16"
  },
  {
    job_id:"17",
    work:"w17",
    po:"po17"
  },
  {
    job_id:"18",
    work:"w18",
    po:"po18"
  },
  {
    job_id:"19",
    work:"w19",
    po:"po19"
  },

]
export const dummyColumnWithChild =[
    {
      sortable: true,
      label: "Sale #",
      value: "sale_id",
      width: "140",
      type: "string",
    },
    {
      sortable: true,
      label: "Date",
      value: "date_sold",
      width: "140",
      type: "date",
    },
    {
      sortable: true,
      label: "Agent Name",
      value: "agent_name",
      width: "130",
      type: "string",
    },
    {
      label:"Parent",
      hasChildren:true,
      width:'200',
      children:[
          {
            sortable: true,
            label: "Branch",
            value: "branch",
            type: "string",
            hasRenderOption:true,
            options: [
              { color: "#409EFF", name: "Cebu Business Park",value: "cebu business park"},
              { color: "#67C23A", name: "IT Park",value: "it park" },
              { color: "#e6a23c", name: "Oakridge",value: "oakridge" },
            ],

          },
          {
            sortable: true,
            label: "Payment Status",
            value: "payment_status",
            type: "string",
            hasRenderOption:true,
            options: [
              { color: "#409EFF", name: "Paid",value:"paid" },
              { color: "#67C23A", name: "UnPaid",value:"unpaid" },
            ],

          },
      ]
    },

    {
      sortable: true,
      label: "Total Sold",
      value: "total_sold",
      width: "140",
      type: "currency",
    },
    {
      sortable: true,
      label: "# of Item Sold",
      value: "item_sold",
      width: "140",
      type: "percent",
    },
    {
      sortable: true,
      label: "Discount",
      value: "discount",
      width: "140",
      type: "voucher",
    },
    {
      sortable: true,
      label: "Employement Type",
      value: "employment_type",
      width: "160",
      type: "string",
      renderSelect: true,
      dropDown: [
        { name: "Fulltime", value: "fulltime" },
        { name: "Outsource", value: "outsource" },
        { name: "Partime", value: "partime" },
      ],
    },
    {
      sortable: true,
      label: "Shift",
      value: "shift",
      type: "string",
      width:"140",
      renderSelect: true,
      dropDown: [
        { name: "DayShift", value: "DayShift" },
        { name: "NightShift", value: "NightShift" },
      ],
    },
  ]

  export const dummyColumn =[
    {
      sortable: true,
      label: "Sale #",
      value: "sale_id",
      type: "string",
    },
    {
      sortable: true,
      label: "Date",
      value: "date_sold",
      type: "date",
    },
    {
      sortable: true,
      label: "Agent Name",
      value: "agent_name",
      type: "string",
    },
    {
      sortable: true,
      label: "Branch",
      value: "branch",
      type: "string",
      width:"180",
      renderSelect: true,
      options: [
        { color: "#409EFF", name: "Cebu Business Park",value: "CEBU BUSINESS PARK"},
        { color: "#67C23A", name: "IT Park",value: "IT park" },
        { color: "#e6a23c", name: "Oakridge",value: "oakridge" },
      ],
    },
    {
      sortable: true,
      label: "Activate",
      value: "activate",
      type: "string",
      width:"180",
      renderSelect: true,
      options: [
        { color: "#409EFF", name: "Activate",optionLabel:'Activated',value: "activate"},
        { color: "#67C23A", name: "Deactivate",optionLabel:'Deactivated',value: "deactivate" },
      ],
    },
    {
      sortable: true,
      label: "Payment Status",
      value: "payment_status",
      type: "string",
      hasRenderOption:true,
      options: [
        { color: "#409EFF", name: "Paid",value:"paid" },
        { color: "#67C23A", name: "UnPaid",value:"unpaid" },
      ],

    },
    {
      sortable: true,
      label: "Total Sold",
      value: "total_sold",
      type: "currency",
    },
    {
      sortable: true,
      label: "# of Item Sold",
      value: "item_sold",
      type: "percent",
    },
    {
      sortable: true,
      label: "Discount",
      value: "discount",
      type: "voucher",
    },
    {
      sortable: true,
      label: "Employement Type",
      value: "employment_type",
      type: "string",
      width:"160",
      renderSelect: true,
      options: [
        { name: "Fulltime", value: "fulltime" },
        { name: "Outsource", value: "outsource" },
        { name: "Partime", value: "partime" },
      ],
    },
    {
      sortable: true,
      label: "Shift",
      value: "shift",
      type: "string",
      width:"160",
      renderSelect: true,
      options: [
        { name: "DayShift", value: "DayShift" },
        { name: "NightShift", value: "NightShift" },
      ],
    },
  ]
  export const dummyColumns =[
    {
      sortable: true,
      label: "Sale #",
      value: "sale_id",
      width: "140",
      type: "string",
    },
    {
      sortable: true,
      label: "Date",
      value: "date_sold",
      width: "140",
      type: "date",
    },
    {
      sortable: true,
      label: "Agent Name",
      value: "agent_name",
      width: "130",
      type: "string",
    },
    {
      sortable: true,
      label: "Branch",
      value: "branch",
      width: "140",
      type: "string",
      hasRenderOption:true,
      options: [
        { color: "#409EFF", name: "Cebu Business Park",value: "CEBU BUSINESS PARK"},
        { color: "#67C23A", name: "IT Park",value: "IT park" },
        { color: "#e6a23c", name: "Oakridge",value: "oakridge" },
      ],
    },
    {
      sortable: true,
      label: "Payment Status",
      value: "payment_status",
      type: "string",
      width: "240",
      hasRenderOption:true,
      options: [
        { color: "#409EFF", name: "Paid",value:"paid" },
        { color: "#67C23A", name: "UnPaid",value:"unpaid" },
      ],

    },
    {
      sortable: true,
      label: "Total Sold",
      value: "total_sold",
      width: "140",
      type: "currency",
    },
    {
      sortable: true,
      label: "# of Item Sold",
      value: "item_sold",
      width: "140",
      type: "percent",
    },
    {
      sortable: true,
      label: "Discount",
      value: "discount",
      width: "140",
      type: "voucher",
    },
    {
      sortable: true,
      label: "Employement Type",
      value: "employment_type",
      width: "260",
      type: "string",
      renderSelect: true,
      options: [
        { name: "Fulltime", value: "fulltime" },
        { name: "Outsource", value: "outsource" },
        { name: "Partime", value: "partime" },
      ],
    },
    {
      sortable: true,
      label: "Shift",
      value: "shift",
      type: "string",
      width:"240",
      renderSelect: true,
      options: [
        { name: "DayShift", value: "DayShift" },
        { name: "NightShift", value: "NightShift" },
      ],
    },
  ]
export const dummyDatas = [
  {
    job_id: "0001",
    team:"one",
    job_type:"fulltime",
    client:"sancaps",
    address:"N/A",
    status:"positive",
  },
  {
    job_id: "0002",
    team:"two",
    job_type:"partime",
    client:"sancaps",
    address:"N/A",
    status:"negative"
  }
]

export const dummyData1 = [
  {
    invoice_id:"012",
    billing_address:"walaaaa",
    invoice_status:"unpaid",
    payment_method:"visa",
    payment_status:"pending",
  },
  {
    invoice_id:"021",
    billing_address:"walaaaa",
    invoice_status:"paid",
    payment_method:"gcash",
    payment_status:"error",
  }
]

export const dummyHeaderData = {
    clickabletags: [
      { color: "#5da8a9", label: "Attendance", count:0 },
      { color: "#409EFF", label: "Cebu Business Park" , count:0},
      { color: "#67C23A", label: "IT Park" ,},
      { color: "#5Fd233", label: "IT Shark" ,},
      { color: "#e6a23c", label: "Oakridge" , count:0},

    ],
    columnName: "branch",
  }
export const  dummyDataPage= [
  {
    sale_id: null,
    date_sold: "",
    agent_name: "",
    branch: null,
    total_sold: null,
    item_sold: "300",
    employment_type: "fulltime",
    shift: "DayShift",
    discount:{
      value:"200",
      type:"flat"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2051",
    date_sold: "2021-08-14",
    agent_name: "John Doe",
    branch: "it park",
    total_sold: 56000,
    item_sold: "300.23",
    employment_type: "partime",
    shift: "NightShift",
    discount:{
      value:"342",
      type:"percent"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2052",
    date_sold: "2021-11-22",
    agent_name: "John Doe",
    branch: "it park",
    total_sold: 8000,
    item_sold: "200.2",
    employment_type: "partime",
    shift: "DayShift",
    discount:{
      value:"420",
      type:"flat"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'deactivate'
  },
  {
    sale_id: "SAL-2032",
    date_sold: "2021-07-13",
    agent_name: "Tony Stark",
    branch: "it park",
    total_sold: 55090,
    item_sold: "500",
    employment_type: "partime",
    shift: "DayShift",
    discount:{
      value:"500",
      type:"flat"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2033",
    date_sold: "2021-07-25",
    agent_name: "Wanda Maximoff",
    branch: "it park",
    total_sold: 15090,
    item_sold: 200,
    employment_type: "outsource",
    shift: "NightShift",
    discount:{
      value:"1200",
      type:"percent"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2034",
    date_sold: "2021-08-10",
    agent_name: "Steve Rogers",
    branch: "oakridge",
    total_sold: 60090,
    item_sold: 650,
    employment_type: "partime",
    shift: "DayShift",
    discount:{
      value:"400",
      type:"flat"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2035",
    date_sold: "2021-09-07",
    agent_name: "Rimuru Tempest",
    branch: "it park",
    total_sold: 1000,
    item_sold: 50,
    employment_type: "outsource",
    shift: "DayShift",
    discount:{
      value:"230",
      type:"flat"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2036",
    date_sold: "2021-10-20",
    agent_name: "Nick Fury",
    branch: "oakridge",
    total_sold: 20000,
    item_sold: 140,
    employment_type: "outsource",
    shift: "NightShift",
    discount:{
      value:"660",
      type:"percent"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
  {
    sale_id: "SAL-2037",
    date_sold: "2021-07-21",
    agent_name: "Bucky Barnes",
    branch: "Cebu Business Park",
    total_sold: 12100,
    item_sold: 210,
    employment_type: "outsource",
    shift: "NightShift",
    discount:{
      value:"660",
      type:"flat"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2038",
    date_sold: "2021-06-30",
    agent_name: "James Carr",
    branch: "it park",
    total_sold: 2300,
    item_sold: 120,
    employment_type: "fulltime",
    shift: "NightShift",
    discount:{
      value:"1230",
      type:"flat"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2039",
    date_sold: "2021-11-01",
    agent_name: "Bill Burr",
    branch: "Cebu Business Park",
    total_sold: -11000,
    item_sold: 200,
    employment_type: "partime",
    shift: "DayShift",
    discount:{
      value:"1230",
      type:"percent"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2040",
    date_sold: "2021-12-25",
    agent_name: "Gabriel Iglesias",
    branch: "it park",
    total_sold: 4300,
    item_sold: 240,
    employment_type: "fulltime",
    shift: "NightShift",
    discount:{
      value:"200",
      type:"percent"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2041",
    date_sold: "2021-11-21",
    agent_name: "Stan Lee",
    branch: "Cebu Business Park",
    total_sold: 2100,
    item_sold: 200,
    employment_type: "fulltime",
    shift: "NightShift",
    discount:{
      value:"990",
      type:"flat"
    },
    payment_status:"paid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2044",
    date_sold: "2021-09-21",
    agent_name: "Bruce Banner",
    branch: "Cebu Business Park",
    total_sold: 1200,
    item_sold: 1300,
    employment_type: "partime",
    shift: "NightShift",
    discount:{
      value:"12300",
      type:"percent"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2045",
    date_sold: "2021-08-11",
    agent_name: "Janet Pym",
    branch: "oakridge",
    total_sold: 3607,
    item_sold: 900,
    employment_type: "outsource",
    shift: "NightShift",
    discount:{
      value:null,
      type:null
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2046",
    date_sold: "2021-07-11",
    agent_name: "Hank Pym",
    branch: "oakridge",
    total_sold: 10000,
    item_sold: 121,
    employment_type: "outsource",
    shift: "DayShift",
    discount:{
      value:"2400",
      type:"flat"
    },
    payment_status:"unpaid",
    job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'

  },
  {
    sale_id: "SAL-2042",
    date_sold: "2021-12-21",
    agent_name: "Dave Chappelle",
    branch: "it park",
    total_sold: 450000,
    item_sold: 200,
    employment_type: "partime",
    shift: "NightShift",
    discount:{
      value:"230",
      type:"percent"
    },
    payment_status:"paid",
        job_id:"19",
    work:"w19",
    po:"po19",
    activate:'deactivated'

  },
  {
    sale_id: "SAL-2043",
    date_sold: "2021-12-13",
    agent_name: "Scott Lang",
    branch: "oakridge",
    total_sold: 6500,
    item_sold: 122,
    employment_type: "outsource",
    shift: "DayShift",
    discount:{
      value:"990",
      type:"flat"
    },
    payment_status:"unpaid",
        job_id:"19",
    work:"w19",
    po:"po19",
    activate:'activate'
  },
]
export const  dummyDataPage1= [
    {
      sale_id: null,
      date_sold: null,
      agent_name: null,
      branch: null,
      total_sold: null,
      item_sold: null,
      employment_type: "fulltime",
      shift: null,
      discount:null,
      payment_status:null,
              job_id:"2031",
    work:"w2031",
    po:"po2031"
    },
    {
      sale_id: "SAL-2051",
      date_sold: "2021-08-14",
      agent_name: "John Doe",
      branch: "it park",
      total_sold: 56000,
      item_sold: "300.23",
      employment_type: "partime",
      shift: "NightShift",
      discount:{
        value:null,
        type:'flat'
      },
      payment_status:"paid",
              job_id:"2051",
    work:"w2051",
    po:"po2051"
    },
    {
      sale_id: "SAL-2052",
      date_sold: "2021-11-22",
      agent_name: "John Doe",
      branch: "it park",
      total_sold: 8000,
      item_sold: "200.2",
      employment_type: "partime",
      shift: "DayShift",
      discount:{
        value:null,
        type:"percent"
      },
      payment_status:"paid",
              job_id:"2052",
    work:"w2052",
    po:"po2052"

    },
    {
      sale_id: "SAL-2032",
      date_sold: "2021-07-13",
      agent_name: "Tony Stark",
      branch: "it park",
      total_sold: 55090,
      item_sold: "500",
      employment_type: "partime",
      shift: "DayShift",
      discount:{
        value:"500",
        type:"flat"
      },
      payment_status:"unpaid",
              job_id:"2032",
    work:"w2032",
    po:"po2032"
    },
    {
      sale_id: "SAL-2033",
      date_sold: "2021-07-25",
      agent_name: "Wanda Maximoff",
      branch: "it park",
      total_sold: 15090,
      item_sold: 200,
      employment_type: "outsource",
      shift: "NightShift",
      discount:{
        value:"1200",
        type:"percent"
      },
      payment_status:"unpaid",
              job_id:"2033",
    work:"w2033",
    po:"po2033"
    },
    {
      sale_id: "SAL-2034",
      date_sold: "2021-08-10",
      agent_name: "Steve Rogers",
      branch: "oakridge",
      total_sold: 60090,
      item_sold: 650,
      employment_type: "partime",
      shift: "DayShift",
      discount:{
        value:"400",
        type:"flat"
      },
      payment_status:"unpaid",
              job_id:"2034",
    work:"w2034",
    po:"po2034"
    },
    {
      sale_id: "SAL-2035",
      date_sold: "2021-09-07",
      agent_name: "Rimuru Tempest",
      branch: "it park",
      total_sold: 1000,
      item_sold: 50,
      employment_type: "outsource",
      shift: "DayShift",
      discount:{
        value:"230",
        type:"flat"
      },
      payment_status:"unpaid",
              job_id:"2035",
    work:"w2035",
    po:"po2035"
    },
    {
      sale_id: "SAL-2036",
      date_sold: "2021-10-20",
      agent_name: "Nick Fury",
      branch: "oakridge",
      total_sold: 20000,
      item_sold: 140,
      employment_type: "outsource",
      shift: "NightShift",
      discount:{
        value:"660",
        type:"percent"
      },
      payment_status:"paid",
              job_id:"2036",
    work:"w2036",
    po:"po2036"
    },
    {
      sale_id: "SAL-2037",
      date_sold: "2021-07-21",
      agent_name: "Bucky Barnes",
      branch: "Cebu Business Park",
      total_sold: 12100,
      item_sold: 210,
      employment_type: "outsource",
      shift: "NightShift",
      discount:{
        value:"660",
        type:"flat"
      },
      payment_status:"paid",
              job_id:"2037",
    work:"w2037",
    po:"po2037"

    },
    {
      sale_id: "SAL-2038",
      date_sold: "2021-06-30",
      agent_name: "James Carr",
      branch: "it park",
      total_sold: 2300,
      item_sold: 120,
      employment_type: "fulltime",
      shift: "NightShift",
      discount:{
        value:"1230",
        type:"flat"
      },
      payment_status:"paid",
              job_id:"2038",
    work:"w2038",
    po:"po2038"

    },
    {
      sale_id: "SAL-2039",
      date_sold: "2021-11-01",
      agent_name: "Bill Burr",
      branch: "Cebu Business Park",
      total_sold: -11000,
      item_sold: 200,
      employment_type: "partime",
      shift: "DayShift",
      discount:{
        value:"1230",
        type:"percent"
      },
      payment_status:"paid",
              job_id:"2039",
    work:"w2039",
    po:"po2039"

    },
    {
      sale_id: "SAL-2040",
      date_sold: "2021-12-25",
      agent_name: "Gabriel Iglesias",
      branch: "it park",
      total_sold: 4300,
      item_sold: 240,
      employment_type: "fulltime",
      shift: "NightShift",
      discount:{
        value:"200",
        type:"percent"
      },
      payment_status:"paid",
              job_id:"2040",
    work:"w2040",
    po:"po2040"

    },
    {
      sale_id: "SAL-2041",
      date_sold: "2021-11-21",
      agent_name: "Stan Lee",
      branch: "Cebu Business Park",
      total_sold: 2100,
      item_sold: 200,
      employment_type: "fulltime",
      shift: "NightShift",
      discount:{
        value:"990",
        type:"flat"
      },
      payment_status:"paid",
              job_id:"2041",
    work:"w2041",
    po:"po2041"

    },
  ]
export const  dummyDataPage2= [
  {
    sale_id: "SAL-2044",
    date_sold: "2021-09-21",
    agent_name: "Bruce Banner",
    branch: "Cebu Business Park",
    total_sold: 1200,
    item_sold: 1300,
    employment_type: "partime",
    shift: "NightShift",
    discount:{
      value:"12300",
      type:"percent"
    },
    payment_status:"unpaid",
    job_id:"Page2-2044",
    work:"Page2-w2044",
    po:"Page2-po2044"

  },
  {
    sale_id: "SAL-2045",
    date_sold: "2021-08-11",
    agent_name: "Janet Pym",
    branch: "oakridge",
    total_sold: 3607,
    item_sold: 900,
    employment_type: "outsource",
    shift: "NightShift",
    discount:{
      value:"350",
      type:"percent"
    },
    payment_status:"unpaid",
                  job_id:"Page2-2040",
    work:"Page2-w2040",
    po:"Page2-po2040"

  },
  {
    sale_id: "SAL-2046",
    date_sold: "2021-07-11",
    agent_name: "Hank Pym",
    branch: "oakridge",
    total_sold: 10000,
    item_sold: 121,
    employment_type: "outsource",
    shift: "DayShift",
    discount:{
      value:"2400",
      type:"flat"
    },
    payment_status:"unpaid",
                  job_id:"Page2-2040",
    work:"Page2-w2040",
    po:"Page2-po2040"

  },
  {
    sale_id: "SAL-2042",
    date_sold: "2021-12-21",
    agent_name: "Dave Chappelle",
    branch: "it park",
    total_sold: 450000,
    item_sold: 200,
    employment_type: "partime",
    shift: "NightShift",
    discount:{
      value:"230",
      type:"percent"
    },
    payment_status:"paid",
                  job_id:"Page2-2040",
    work:"Page2-w2040",
    po:"Page2-po2040"

  },
  {
    sale_id: "SAL-2043",
    date_sold: "2021-12-13",
    agent_name: "Scott Lang",
    branch: "oakridge",
    total_sold: 6500,
    item_sold: 122,
    employment_type: "outsource",
    shift: "DayShift",
    discount:{
      value:"990",
      type:"flat"
    },
    payment_status:"unpaid",
                  job_id:"Page2-2040",
    work:"Page2-w2040",
    po:"Page2-po2040"

  },
  ]
